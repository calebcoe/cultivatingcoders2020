# Overview

The purpose of this repository is to track my progress throughout my studies.


## Design Projects
- Moodboard
- Circus mockup
- Webpage redesign
- Wireframing
- Mobile webpage redesign


#### Moodboard
The purpose of this assignment was to invoke feelings with a collage of photos that represent the business.
My moodboards are set for the local companies: Woodworker's Supply and J. Gumbo's.

#### Circus mockup
The purpose of this assignment was to learn the basics of Adobe Xd by recreating a webpage designed by one of our instructors.

#### Webpage redesign
The purpose of this assignment was to further practice Abobe Xd. I decided to redesign the webpage for an Australian based tea 
company called T2. Their website is already designed quite well, but it is not clear right away that this company is actually 
pushing for more sustainability. My redesign sligtly tweaks the color scheme, and aims to rebrand them as more of a "green" company.

#### Wireframing
The purpose of wireframing is to set a blueprint for the overall layout of the page. It will help shave off time when it comes to 
making the actual design of the page using Adobe Xd.

#### Mobile webpage redesign
The purpose of this assignment was to take our previous redesign and make it more responsive by adding a mobile version.
